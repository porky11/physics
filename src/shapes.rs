use num_traits::One;

pub struct Ball<T> {
    pub rad: T
}

impl<T: One> Default for Ball<T> {
    fn default() -> Self {
        Self {
            rad: T::one()
        }
    }
}

pub struct Convex2<T> {
    points: Vec<[T;2]>,
    indices: Vec<[usize;2]>
}

impl<T> Default for Convex2<T> {
    fn default() -> Self {
        Self {
            points: Vec::new(),
            indices: Vec::new(),
        }
    }
}


pub struct Convex3<T> {
    points: Vec<[T;3]>,
    indices: Vec<[usize;3]>
}

impl<T> Default for Convex3<T> {
    fn default() -> Self {
        Self {
            points: Vec::new(),
            indices: Vec::new(),
        }
    }
}

pub struct CollisionData<T> {
    pub vector: T,
    pub point_self: T,
    pub point_other: T
}

pub trait CollisionInfo<P,S> {
    fn collision_info(&self, other: &S, self_pos: P, other_pos: P) -> Option<CollisionData<P>>;
}

use vector_space::{InnerSpace,VectorSpace};

use std::ops::Neg;

use num_traits::real::Real;

impl<P: Neg<Output=P>+InnerSpace+Copy> CollisionInfo<P,Ball<<P as VectorSpace>::Scalar>>
    for Ball<<P as VectorSpace>::Scalar>
        where <P as VectorSpace>::Scalar: Real {
    fn collision_info(&self,other: &Ball<<P as VectorSpace>::Scalar>,self_pos: P,other_pos: P) -> Option<CollisionData<P>> {
        let disvec = other_pos - self_pos;
        let dis = disvec.magnitude();
        let srad = self.rad + other.rad;
        if dis < srad {
            let dir = disvec/dis;
            let vector = -dir*(srad-dis);
            let point_self = dir*self.rad;
            let point_other = -dir*other.rad;
            let data = CollisionData {
                vector,
                point_self,
                point_other,
            };
            Some(data)
        } else {
            None
        }
    }
}
