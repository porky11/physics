extern crate ga;

use self::ga::{
    Bivector2,
    Rotor2,
    Bivector3,
    Rotor3
};

use transform::Transform;

use std::ops::Neg;
use num_traits::Num;

impl<T: Num+Neg<Output=T>+Copy> Transform<Bivector2<T>> for Rotor2<T> {
    fn transform(&mut self, other: Bivector2<T>) {
        *self = *self*other;
    }
}

impl<T: Num+Neg<Output=T>+Copy> Transform<Bivector3<T>> for Rotor3<T> {
    fn transform(&mut self, other: Bivector3<T>) {
        *self = *self*other;
    }
}


