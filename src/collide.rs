pub trait Collide<T: Collide<Self> = Self>: Sized {
    #[inline]
    fn collide(&mut self, other: &mut T) {
        other.collide(self);
    }
}


