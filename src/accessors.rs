pub trait Position {
    type Type;
    fn pos(&self) -> &Self::Type;
}

pub trait Velocity {
    type Type;
    fn vel(&self) -> &Self::Type;
}

pub trait Orientation {
    type Type;
    fn dir(&self) -> &Self::Type;
}

pub trait Rotation {
    type Type;
    fn rot(&self) -> &Self::Type;
}


