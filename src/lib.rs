extern crate num_traits;
extern crate vector_space;

pub mod shapes;
pub mod accessors;
pub mod transform;
pub mod collide;
#[cfg(feature = "gaimpl")]
pub mod ga;

