use std::ops::AddAssign;
use std::ops::Neg;

pub trait Transform<T> {
    fn transform(&mut self, T);
}

impl<T> Transform<T> for T where T: AddAssign<T> {
    fn transform(&mut self, value: T) {
        *self += value
    }
}

pub trait Accelerate<T> {
    fn accelerate(&mut self, acc: T);
    fn accelerate2(&mut self, other: &mut Self, acc: T) where T: Copy+Neg<Output=T> {
        self.accelerate(acc);
        other.accelerate(-acc);
    }
}


